#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""this script computes the length of transcipts from a gtf file"""

import re



def get_transcript_length(input_file=None):
    """get start and end of transcript to calculate length"""
    tx_start = dict()
    tx_end = dict()
    file_handler = open(input_file)
    for line in file_handler:
        token = line.split("\t")
        start = int(token[3])  # le début de l'élément courant
        end = int(token[4])  # la fin de l'élément courant
        # L'identifiant du transcrit
        tx_id = re.search('transcript_id "([^"]+)"', token[8]).group(1)

        if tx_id not in tx_start:

            tx_start[tx_id] = start
            tx_end[tx_id] = end

        else:

            if start < tx_start[tx_id]:
                tx_start[tx_id] = start

            if end > tx_end[tx_id]:
                tx_end[tx_id] = end

    for tx_id in tx_start:
        print(tx_id + "\t" + str(tx_end[tx_id] - tx_start[tx_id] + 1))


if __name__ == '__main__':
    get_transcript_length(input_file='../pymetacline/data/gtf/simple.gtf')
