#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''this script allows the selection of sequences from a fasta file by entering
the transcript id as an argument'''

import argparse
from Bio import SeqIO

def select_sequence(args):
    '''my beautiful function to select sequences from a fasta file'''
    fasta = SeqIO.parse(args.inputfile, 'fasta')
    new_fasta = []

    for record in fasta:
        if record.id in args.sequenceid:
            new_fasta.append(record.id)
            new_fasta.append(record.seq)
    return new_fasta

def create_parser():
    '''creation of the parser arguments'''
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputfile', type=str, help='Path to the fasta file')
    parser.add_argument('-seq', '--sequenceid', nargs='+', type=str, help=\
    'enter transcript id separated by commas. Transcript id must start with \
    ENST followed by 11 digits')
    return parser

def main():
    '''launcher of the argparse'''
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args['input_file'])

if __name__ == "__main__":
    main()
    